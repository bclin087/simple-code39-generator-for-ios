//
//  AppDelegate.h
//  Code39Test
//
//  Created by Lin Patrick on 10/17/15.
//  Copyright © 2015 Gemmy Planet, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

